package br.edu.up.sensores;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

public class ListaDeSensoresActivity extends AppCompatActivity {


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_lista_de_sensores);

    TextView txt = (TextView) findViewById(R.id.textView);

    SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    List<Sensor> sensores = sensorManager.getSensorList(Sensor.TYPE_ALL);

    StringBuilder mensagem = new StringBuilder();
    SensorUtil sensorUtil = new SensorUtil();

    for (Sensor sensor : sensores) {
      mensagem.append(sensor.getName() + "\r\n");
      mensagem.append(sensorUtil.get(sensor.getType()) + "\r\n\n");
    }
    txt.setText(mensagem.toString());

  }

  public static boolean isAccelerometerSupported(Context context) {
    SensorManager sm =
        (SensorManager) context
            .getSystemService(Context.SENSOR_SERVICE);
    List<Sensor> sensors = sm.getSensorList(Sensor.TYPE_ACCELEROMETER);
    return sensors.size() > 0;
  }

  public class SensorUtil extends HashMap<Integer, String> {

    public SensorUtil() {
      put(1, "TYPE_ACCELEROMETER");
      put(-1, "TYPE_ALL");
      put(13, "TYPE_AMBIENT_TEMPERATURE");
      put(65536, "TYPE_DEVICE_PRIVATE_BASE");
      put(15, "TYPE_GAME_ROTATION_VECTOR");
      put(20, "TYPE_GEOMAGNETIC_ROTATION_VECTOR");
      put(9, "TYPE_GRAVITY");
      put(4, "TYPE_GYROSCOPE");
      put(16, "TYPE_GYROSCOPE_UNCALIBRATED");
      put(31, "TYPE_HEART_BEAT");
      put(21, "TYPE_HEART_RATE");
      put(5, "TYPE_LIGHT");
      put(10, "TYPE_LINEAR_ACCELERATION");
      put(2, "TYPE_MAGNETIC_FIELD");
      put(14, "TYPE_MAGNETIC_FIELD_UNCALIBRATED");
      put(30, "TYPE_MOTION_DETECT");
      put(3, "TYPE_ORIENTATION");
      put(28, "TYPE_POSE_6DOF");
      put(6, "TYPE_PRESSURE");
      put(8, "TYPE_PROXIMITY");
      put(12, "TYPE_RELATIVE_HUMIDITY");
      put(11, "TYPE_ROTATION_VECTOR");
      put(17, "TYPE_SIGNIFICANT_MOTION");
      put(29, "TYPE_STATIONARY_DETECT");
      put(19, "TYPE_STEP_COUNTER");
      put(18, "TYPE_STEP_DETECTOR");
      put(7, "TYPE_TEMPERATURE");
    }
  }
}
